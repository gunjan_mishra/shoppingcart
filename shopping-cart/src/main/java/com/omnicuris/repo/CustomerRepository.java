package com.omnicuris.repo;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.omnicuris.model.Customers;

@Repository
public interface CustomerRepository extends JpaRepository<Customers, Serializable> {


}
