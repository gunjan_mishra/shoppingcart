package com.omnicuris.repo;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.omnicuris.model.CustomerOrders;

@Repository
public interface CustomerOrderRepository extends JpaRepository<CustomerOrders, Serializable> {
	
	@Query("SELECT CORD FROM CustomerOrders CORD WHERE customer_id.email = :email")
	List<CustomerOrders>findCustomerOrdersBycustomer_idEmail(@Param("email")String email);

}
