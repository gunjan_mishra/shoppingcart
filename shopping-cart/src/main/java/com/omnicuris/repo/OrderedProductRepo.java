package com.omnicuris.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.omnicuris.model.OrderedProduct;

@Repository
public interface OrderedProductRepo extends JpaRepository<OrderedProduct, Integer>{


}
