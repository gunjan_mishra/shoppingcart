package com.omnicuris.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.omnicuris.model.Customers;
import com.omnicuris.model.Product;
import com.omnicuris.repo.CustomerRepository;
import com.omnicuris.vo.CustomerBeanInput;
import com.omnicuris.vo.Result;

@Service

public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository customerRepo;

	@Override
	public Result addCustomer(CustomerBeanInput customerBeanInput) {

		Customers customers = new Customers();
		customers.setName(customerBeanInput.getName());
		customers.setAddress(customerBeanInput.getAddress());
		customers.setEmail(customerBeanInput.getEmail());
		customers.setPhone(customerBeanInput.getPhone());
		customerRepo.save(customers);
		Result result = new Result(customers, "CUSTOMER-CREATED");
		return result;
	}

	@Override
	public Result getCustomerList() {
		List<Customers> customerList = customerRepo.findAll();
		Result result = new Result(customerList, "CUSTOMER-RETRIEVED");
		return result;
	}

	@Override
	public Result updateCustomer(int id, CustomerBeanInput customerBeanInput) {
		Optional<Customers> optcustomer = customerRepo.findById(id);
		Customers customers = optcustomer.get();
		customers.setAddress(customerBeanInput.getAddress());
		customers.setEmail(customerBeanInput.getEmail());
		customers.setName(customerBeanInput.getName());
		customers.setPhone(customerBeanInput.getPhone());
		customerRepo.save(customers);
		Result result = new Result(customers, "CUSTOMER-UPDATED");
		return result;
	}

	@Override
	public Result deleteCustomer(int id) {
		customerRepo.deleteById(id);
		Result result=new Result("customer id "+ id +"Delete successfully");
	    return result;
		}
}


