package com.omnicuris.service;

import com.omnicuris.vo.CustomerBeanInput;
import com.omnicuris.vo.Result;

public interface CustomerService {

	public Result addCustomer(CustomerBeanInput customerBeanInput);

	public Result getCustomerList();

	public Result updateCustomer(int id, CustomerBeanInput customerBeanInput);

	public Result deleteCustomer(int id);
	
	
	
	

}
