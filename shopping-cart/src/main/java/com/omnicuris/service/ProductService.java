package com.omnicuris.service;

import com.omnicuris.vo.ProductBeanInput;
import com.omnicuris.vo.Result;

public interface ProductService {
	
	public Result addProduct(ProductBeanInput productBeanInput);
	
	public Result getProductList();

	public Result updateProduct(int id,ProductBeanInput productBeanInput);

	public Result deleteProduct(int id);

}
