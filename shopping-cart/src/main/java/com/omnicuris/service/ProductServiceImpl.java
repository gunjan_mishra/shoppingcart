package com.omnicuris.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.omnicuris.model.Product;
import com.omnicuris.repo.ProductRepository;
import com.omnicuris.vo.ProductBeanInput;
import com.omnicuris.vo.Result;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepo;

	@Override
	public Result addProduct(ProductBeanInput productBeanInput) {
		Product product = new Product();
		product.setName(productBeanInput.getName());
		product.setPrice(productBeanInput.getPrice());
		product.setLast_update(productBeanInput.getLast_update());
		productRepo.save(product);
		Result result=new Result(product," PRODUCT-CREATED");
        return result;
	}
	
	

	@Override
	public Result getProductList() {
		List<Product> productList=productRepo.findAll();
		Result result=new Result(productList,"PRODUCT-RETRIEVED");
		 return result;
	}
	
	@Override
	public Result updateProduct(int id, ProductBeanInput productBeanInput) {
		Optional<Product> optproduct=productRepo.findById(id);
		Product product=optproduct.get();
		product.setName(productBeanInput.getName());
		product.setLast_update(productBeanInput.getLast_update());
		product.setPrice(productBeanInput.getPrice());
		productRepo.save(product);
		Result result=new Result(product,"PRODUCT-UPDATED");
        return result;
	}
	
	@Override
	public Result deleteProduct(int id ) {
		productRepo.deleteById(id);
		Result result=new Result(null,"produuct id "+ id +"Delete successfully");
        return result;
	}

}
