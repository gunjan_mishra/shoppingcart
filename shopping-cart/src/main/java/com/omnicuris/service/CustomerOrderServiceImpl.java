package com.omnicuris.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.omnicuris.model.CustomerOrders;
import com.omnicuris.model.Customers;
import com.omnicuris.model.OrderedProduct;
import com.omnicuris.model.Product;
import com.omnicuris.repo.CustomerOrderRepository;
import com.omnicuris.repo.CustomerRepository;
import com.omnicuris.repo.OrderedProductRepo;
import com.omnicuris.repo.ProductRepository;
import com.omnicuris.vo.CustomersOrderBeanInput;
import com.omnicuris.vo.OrderedProductBeanInput;
import com.omnicuris.vo.Result;

@Service
public class CustomerOrderServiceImpl implements CustomerOrderService {

	@Autowired
	CustomerOrderRepository customerOrderRepo;

	@Autowired
	CustomerRepository custRepo;

	@Autowired
	ProductRepository productRepo;

	@Autowired
	OrderedProductRepo orderedProductRepo;

	@Override
	public Result createOrder(CustomersOrderBeanInput customerorderBeanInput) {
		Result result = null;
		Optional<Customers> customer = custRepo.findById(customerorderBeanInput.getCustomer_id());
		if (customer.isPresent()) {
			CustomerOrders customerOrders = new CustomerOrders();
			customerOrders.setConfirmation_no(customerorderBeanInput.getConfirmation_no());
			customerOrders.setCreated_date(customerorderBeanInput.getCreated_date());
			customerOrders.setCustomer_id(customer.get());
			customerOrders.setPrice(customerorderBeanInput.getPrice());
			customerOrderRepo.save(customerOrders);
			result = new Result(customerOrders, "ORDER-CREATED");
		}
		return result;
	}

	@Override
	public Result checkoutOrder(OrderedProductBeanInput orderedProductBeanInput) {
		Result result = null;
		Optional<Customers> customer = custRepo.findById(orderedProductBeanInput.getCustomer_id());
		Optional<Product> product = productRepo.findById(orderedProductBeanInput.getProduct_id());

		if ((customer.isPresent()) && (product.isPresent())) {
			OrderedProduct orderedProduct = new OrderedProduct();
			orderedProduct.setCustomer(customer.get());
			orderedProduct.setOrder(product.get());
			orderedProduct.setQuantity(orderedProduct.getQuantity());
			orderedProductRepo.save(orderedProduct);
			result = new Result(orderedProduct, "CHECKOUT");
		}

		return result;
	}

	@Override
	public Result getOrderedProductList() {
		List<CustomerOrders> orderedProductList = customerOrderRepo.findAll();
		Result result = new Result(orderedProductList, "RETRIEVED_ORDERED_PRODUCT");
		return result;
	}

	@Override
	public Result getOrderedProduct(String email) {
		List<CustomerOrders> customerOrders=customerOrderRepo.findCustomerOrdersBycustomer_idEmail(email);
		Result result=new Result(customerOrders,"Retrive By Email Response");
		return result;
	}
}

