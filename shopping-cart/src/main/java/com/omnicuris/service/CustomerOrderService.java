package com.omnicuris.service;

import com.omnicuris.vo.CustomersOrderBeanInput;
import com.omnicuris.vo.OrderedProductBeanInput;
import com.omnicuris.vo.Result;

public interface CustomerOrderService {

	public Result createOrder(CustomersOrderBeanInput customerorderBeanInput);

	public Result checkoutOrder(OrderedProductBeanInput orderedProductBeanInput);

	public Result getOrderedProductList();

	public Result getOrderedProduct(String email);
	

}
