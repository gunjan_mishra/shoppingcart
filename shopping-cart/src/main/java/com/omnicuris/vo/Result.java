package com.omnicuris.vo;

public class Result {

	
	
	private Object object;
	
	private String message;
	

	public Result(Object object) {
		this.object = object;
	}

	public Result(Object object, String message) {
		this.object = object;
		this.message = message;
	}

	public Object getObject() {
		return object;
	}

	

	public String getMessage() {
		return message;
	}

	
	
	
	
	
	
	
}
