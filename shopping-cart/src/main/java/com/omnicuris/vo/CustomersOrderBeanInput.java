package com.omnicuris.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

public class CustomersOrderBeanInput {

	private double price;
	
	@JsonFormat(shape=Shape.STRING,pattern="dd-MM-yyyy")
	private Date created_date;
	private Integer customer_id;
	private int confirmation_no;

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	

	public Integer getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Integer customer_id) {
		this.customer_id = customer_id;
	}

	public int getConfirmation_no() {
		return confirmation_no;
	}

	public void setConfirmation_no(int confirmation_no) {
		this.confirmation_no = confirmation_no;
	}

}
