package com.omnicuris.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import lombok.Data;


public class ProductBeanInput {
		
		private String name;
		private double price;
		@JsonFormat(shape=Shape.STRING,pattern="dd-MM-yyyy")
		private Date last_update;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		public Date getLast_update() {
			return last_update;
		}
		public void setLast_update(Date last_update) {
			this.last_update = last_update;
		}
		
		

	}



