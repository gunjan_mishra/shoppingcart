package com.omnicuris.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

//@Data
@Entity
//@NoArgsConstructor
public class CustomerOrders {
	
	

	public CustomerOrders() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "PRICE")
	private double price;

	@Column(name = "CREATED_DATE")
	private Date created_date;

	@ManyToOne
	private Customers customer_id;

	@Column(name = "CONFIRMATION_NO")
	private int confirmation_no;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Customers getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Customers customer_id) {
		this.customer_id = customer_id;
	}

	public int getConfirmation_no() {
		return confirmation_no;
	}

	public void setConfirmation_no(int confirmation_no) {
		this.confirmation_no = confirmation_no;
	}

	@Override
	public String toString() {
		return "CustomerOrders [id=" + id + ", price=" + price + ", created_date=" + created_date + ", customer_id="
				+ customer_id + ", confirmation_no=" + confirmation_no + "]";
	}

}