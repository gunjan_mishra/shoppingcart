package com.omnicuris.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
//@NoArgsConstructor
//@Data
public class OrderedProduct {
	
	
	
	public OrderedProduct() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private  Integer id;
	
	
	@ManyToOne
	private Customers customer;
	
	
	@ManyToOne
	private Product order;
	
	@Column(name="QUANTITY")
	private Integer quantity;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Customers getCustomer() {
		return customer;
	}

	public void setCustomer(Customers customer) {
		this.customer = customer;
	}

	public Product getOrder() {
		return order;
	}

	public void setOrder(Product order) {
		this.order = order;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "OrderedProduct [id=" + id + ", customer=" + customer + ", order=" + order + ", quantity=" + quantity
				+ "]";
	}
	
	
	
	

}
