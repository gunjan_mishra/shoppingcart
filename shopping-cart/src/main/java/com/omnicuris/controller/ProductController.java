package com.omnicuris.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omnicuris.model.Product;
import com.omnicuris.service.ProductService;
import com.omnicuris.vo.ProductBeanInput;
import com.omnicuris.vo.Result;

@RestController
@RequestMapping(value="/api/product/")
public class ProductController { 
	
	@Autowired
	ProductService productService;
	
	
	@PostMapping(value="add-product",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public Result addProduct(@RequestBody ProductBeanInput productBeanInput) {
		return productService.addProduct(productBeanInput);
		
	}
	
	@GetMapping(value="get-productlist",produces=MediaType.APPLICATION_JSON_VALUE)
	public Result getProductList() {
		return productService.getProductList();
		
	}
	
	@PutMapping(value="update-product/{id}",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public Result updateProduct(@PathVariable int id,@RequestBody ProductBeanInput productBeanInput) {
		return productService.updateProduct(id, productBeanInput);
		
	}
	
	@DeleteMapping(value="delete-product/{id}")
	public Result deleteProduct(@PathVariable int id ) {
		return productService.deleteProduct(id);
	}
	
    
}
