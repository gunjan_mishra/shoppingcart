package com.omnicuris.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.omnicuris.service.CustomerOrderService;
import com.omnicuris.vo.CustomerBeanInput;
import com.omnicuris.vo.CustomersOrderBeanInput;
import com.omnicuris.vo.OrderedProductBeanInput;
import com.omnicuris.vo.Result;

@RestController
@RequestMapping("/api/customer-order/")
public class CustomersOrderController {

	@Autowired
	private CustomerOrderService customerOrderService;

	@PostMapping(value = "create-order", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Result createOrder(@RequestBody CustomersOrderBeanInput customerorderBeanInput) {

		return customerOrderService.createOrder(customerorderBeanInput);

	}

	@PostMapping(value = "checkout-order", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Result checkoutOrder(@RequestBody OrderedProductBeanInput orderdProductBeanInput) {
		System.out.println("orderdProductBeanInput"+orderdProductBeanInput);
		return customerOrderService.checkoutOrder(orderdProductBeanInput);
	}

	@GetMapping(value = "ordered-Product-List", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Result getOrderedProductList() {
		return customerOrderService.getOrderedProductList();

	}

	@GetMapping(value = "get-ordered-product/{email}",  produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Result getOrderedProduct(@PathVariable("email") String email) {
		return customerOrderService.getOrderedProduct(email);

	}

}