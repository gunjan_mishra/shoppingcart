package com.omnicuris.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omnicuris.service.CustomerService;
import com.omnicuris.vo.CustomerBeanInput;
import com.omnicuris.vo.Result;

@RestController
@RequestMapping("/api/customer/")
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@PostMapping(value = "add-customer", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Result addCustomer(@RequestBody CustomerBeanInput customerBeanInput) {
		return customerService.addCustomer(customerBeanInput);

	}

	@GetMapping(value = "get-customerList", produces = MediaType.APPLICATION_JSON_VALUE)
	public Result getCustomerList() {
		return customerService.getCustomerList();
	}

	@PutMapping(value = "update-customer/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Result updateProduct(@PathVariable int id, @RequestBody CustomerBeanInput customerBeanInput) {
		return customerService.updateCustomer(id, customerBeanInput);

	}

	@DeleteMapping(value = "delete-customer/{id}")
	public Result deleteCustomers(@PathVariable int id) {
		return customerService.deleteCustomer(id);
	}

}
